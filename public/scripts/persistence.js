class Persistence {
    static load(itemName, factory) {
        try {
            return JSON.parse(localStorage.getItem(itemName));
        } catch (err) {
            let result = factory();
            Persistence.save(itemName, result);
        }
    }
    static save(itemName, payload) {
        localStorage.setItem(itemName, JSON.stringify(payload));
    }
}