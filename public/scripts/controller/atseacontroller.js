class AtSeaController {
    static startChangeHeading() {
        let world = World.load();
        if (world.avatar.state == Constants.states.atSea) {
            World.clearMessages(world);
            World.addMessage("To the rudder!", world);
            world.avatar.state = Constants.states.changingHeading;
            World.save(world);
            View.refresh();
        }
    }
    static startChangeSpeed() {
        let world = World.load();
        if (world.avatar.state == Constants.states.atSea) {
            World.clearMessages(world);
            World.addMessage("To the sails!", world);
            world.avatar.state = Constants.states.changingSpeed;
            World.save(world);
            View.refresh();
        }
    }
    static move() {
        let world = World.load();
        if (world.avatar.state == Constants.states.atSea) {
            World.clearMessages(world);
            Avatar.move(world.avatar);
            World.addMessage("You move to (" + world.avatar.x.toFixed(2) + "," + world.avatar.y.toFixed(2) + ").", world);
            EventController.doEvent(Constants.events.turn, world);
            World.save(world);
            View.refresh();
        }
    }
}