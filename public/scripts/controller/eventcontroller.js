class EventController {
    static doEvent(eventName, world) {
        if (eventName == Constants.events.turn) {
            CounterHolder.changeCounter(Constants.counters.turns, 1, world);
        }
    }
}