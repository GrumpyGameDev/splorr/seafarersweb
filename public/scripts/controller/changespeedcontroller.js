class ChangeSpeedController {
    static makeItSo() {
        let world = World.load();
        if (world.avatar.state == Constants.states.changingSpeed) {
            World.clearMessages(world);

            world.avatar.speed = Number(document.getElementById("newSpeed").value);
            World.addMessage("Speed now set to " + world.avatar.speed + ".", world);

            world.avatar.state = Constants.states.atSea;

            World.save(world);
            View.refresh();
        }
    }
    static belay() {
        let world = World.load();
        if (world.avatar.state == Constants.states.changingSpeed) {
            World.clearMessages(world);
            World.addMessage("Belay that!", world);
            world.avatar.state = Constants.states.atSea;
            World.save(world);
            View.refresh();
        }
    }
};