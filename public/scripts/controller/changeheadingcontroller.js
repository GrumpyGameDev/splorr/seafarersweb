class ChangeHeadingController {
    static makeItSo() {
        let world = World.load();
        if (world.avatar.state == Constants.states.changingHeading) {
            World.clearMessages(world);

            let newDegrees = Number(document.getElementById("newDegrees").value);
            let newMinutes = Number(document.getElementById("newMinutes").value);
            let newSeconds = Number(document.getElementById("newSeconds").value);

            world.avatar.heading = Utility.ofDMS(newDegrees, newMinutes, newSeconds);
            World.addMessage("Changed heading to " + newDegrees + "&deg; " + newMinutes + "&apos; " + newSeconds + "&quot;.", world);

            world.avatar.state = Constants.states.atSea;
            World.save(world);
            View.refresh();
        }
    }
    static belay() {
        let world = World.load();
        if (world.avatar.state == Constants.states.changingHeading) {
            World.clearMessages(world);
            World.addMessage("Belay that", world);
            world.avatar.state = Constants.states.atSea;
            World.save(world);
            View.refresh();
        }
    }
}