class AtSeaView {
    static getContent(world) {
        let avatar = world.avatar;
        let content = "<h1>At Sea</h1>";
        content += "<p>Turn: " + CounterHolder.getCounter(Constants.counters.turns, world) + "</p>";
        for (let i in world.messages) {
            content += "<p><i>" + world.messages[i] + "</i></p>";
        }
        content += "<p>Position: (" + avatar.x.toFixed(2) + "," + avatar.y.toFixed(2) + ") <button onclick=\"AtSeaController.move()\">Sail!</button></p>";
        let dms = Utility.toDMS(avatar.heading);
        content += "<p>Heading: " + dms.degrees + "&deg; " + dms.minutes + "&apos; " + dms.seconds + "&quot; <button onclick=\"AtSeaController.startChangeHeading()\">Change...</button></p>"
        content += "<p>Speed: " + avatar.speed + " <button onclick=\"AtSeaController.startChangeSpeed()\">Change...</button></p>"
        return content;
    }
};