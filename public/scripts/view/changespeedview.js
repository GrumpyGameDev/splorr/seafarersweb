class ChangeSpeedView {
    static getContent(world) {
        let avatar = world.avatar;
        let content = "<h1>Change Speed</h1>";
        for (let i in world.messages) {
            content += "<p><i>" + world.messages[i] + "</i></p>";
        }
        content += "<p>Current Speed: " + avatar.speed + "</p>"
        content += "<p>New Speed: <select id=\"newSpeed\">"
        content += "<option value=\"0\">All stop</option>"
        content += "<option value=\"0.3\">Ahead 1/3</option>"
        content += "<option value=\"0.6\">Ahead 2/3</option>"
        content += "<option value=\"0.9\" selected=\"selected\">Ahead full</option>"
        content += "<option value=\"1\">Ahead flank</option>"
        content += "</select> <button onclick=\"ChangeSpeedController.makeItSo()\">Make it so!</button></p>"
        content += "<p><button onclick=\"ChangeSpeedController.belay()\">Belay that!</button></p>";
        return content;
    }
};