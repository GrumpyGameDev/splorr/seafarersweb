class View {
    static get debug() {
        return true;
    }
    static refresh() {
        var content = "";
        var world = World.load();
        if (world.avatar.state == Constants.states.atSea) {
            content = AtSeaView.getContent(world);
        } else if (world.avatar.state == Constants.states.changingHeading) {
            content = ChangeHeadingView.getContent(world);
        } else if (world.avatar.state == Constants.states.changingSpeed) {
            content = ChangeSpeedView.getContent(world);
        }
        if (View.debug) {
            content += "<hr/><p><button onclick=\"World.reset();View.refresh();\">Reset</button></p>";
        }
        document.body.innerHTML = content;
    }
}