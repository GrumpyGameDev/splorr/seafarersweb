class ChangeHeadingView {
    static getContent(world) {
        let avatar = world.avatar;
        let content = "<h1>Change Heading</h1>";
        for (let i in world.messages) {
            content += "<p><i>" + world.messages[i] + "</i></p>";
        }
        var dms = Utility.toDMS(avatar.heading);
        content += "<p>Current Heading: " + dms.degrees + "&deg; " + dms.minutes + "&apos; " + dms.seconds + "&quot;</p>"

        content += "<p>New Heading: <select id=\"newDegrees\">";
        for (let degrees = 0; degrees < 360; ++degrees) {
            content += "<option value=\"" + degrees + "\"";
            if (degrees == dms.degrees) {
                content += " selected=\"selected\"";
            }
            content += ">" + degrees + "&deg;</option>";
        }
        content += "</select><select id=\"newMinutes\">";
        for (let minutes = 0; minutes < 60; ++minutes) {
            content += "<option value=\"" + minutes + "\"";
            if (minutes == dms.minutes) {
                content += " selected=\"selected\"";
            }
            content += ">" + minutes + "&deg;</option>";
        }
        content += "</select><select id=\"newSeconds\">";
        for (let seconds = 0; seconds < 60; ++seconds) {
            content += "<option value=\"" + seconds + "\"";
            if (seconds == dms.seconds) {
                content += " selected=\"selected\"";
            }
            content += ">" + seconds + "&deg;</option>";
        }
        content += "</select><button onclick=\"ChangeHeadingController.makeItSo()\">Make it so!</button></p>";
        content += "<p><button onclick=\"ChangeHeadingController.belay()\">Belay that!</button></p>";
        return content;
    }
};