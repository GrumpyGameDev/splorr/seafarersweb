class Utility {
    static normalizeRadians(rads) {
        var result = Math.atan2(Math.sin(rads), Math.cos(rads));
        if (result < 0) {
            return result + Math.PI * 2;
        } else {
            return result;
        }
    }
    static ofDMS(degrees, minutes, seconds) {
        let temp = seconds / 60.0 + minutes;
        temp /= 60.0
        temp += degrees;
        return temp * Math.PI / 180.0
    }
    static toDMS(rads) {
        rads = Utility.normalizeRadians(rads);
        var temp = rads * 180 / Math.PI;
        var wholeDegrees = Math.floor(temp);
        temp -= wholeDegrees;
        temp *= 60;
        var wholeMinutes = Math.floor(temp);
        temp -= wholeMinutes;
        temp *= 60;
        var wholeSeconds = Math.floor(temp);
        return {
            "degrees": wholeDegrees,
            "minutes": wholeMinutes,
            "seconds": wholeSeconds
        };
    }
    static randomInt(minimum, maximum) {
        return Math.floor(Math.random() * (maximum - minimum + 1) + minimum);
    }
    static randomRange(minimum, maximum) {
        return Math.random() * (maximum - minimum) + minimum;
    }
    static distance(x1, y1, x2, y2) {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }
    static pickRandom(fromArray) {
        return fromArray[Utility.randomInt(0,fromArray.length-1)];
    }
}