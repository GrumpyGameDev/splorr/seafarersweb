class StateConstants {
    get atSea() {
        return "at-sea";
    }
    get changingHeading() {
        return "changing-heading";
    }
    get changingSpeed() {
        return "changing-speed";
    }
}
class StorageConstants {
    get world() {
        return "world";
    }
    get meta() {
        return "meta";
    }
}
class WorldConstants {
    get width() {
        return 100.0;
    }
    get height() {
        return 100.0;
    }
}
class GameConstants {
    get version() {
        return 1.0;
    }
}
class EventConstants {
    get turn() {
        return "turn";
    }
}
class CounterConstants {
    get turns() {
        return "turns";
    }
}
class Constants {
    static get game() {
        return new GameConstants();
    }
    static get world() {
        return new WorldConstants();
    }
    static get states() {
        return new StateConstants();
    }
    static get storage() {
        return new StorageConstants();
    }
    static get events() {
        return new EventConstants();
    }
    static get counters() {
        return new CounterConstants();
    }
}
