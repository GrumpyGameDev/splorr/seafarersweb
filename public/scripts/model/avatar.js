class AvatarData {
    constructor() {
        this.state = Constants.states.atSea;
        this.x = Constants.world.width / 2.0;
        this.y = Constants.world.height / 2.0;
        this.heading = Math.random() * Math.PI * 2.0;
        this.counters = {};
        this.speed = 1.0;
    }
}
class Avatar {
    static create() {
        return new AvatarData();
    }
    static move(avatar) {
        let deltaX = avatar.speed * Math.cos(avatar.heading);
        let deltaY = avatar.speed * Math.sin(avatar.heading);
        avatar.x += deltaX;
        avatar.y += deltaY;
    }
}