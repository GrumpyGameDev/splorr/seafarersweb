class IslandData {
    constructor(name, x, y) {
        this.x = x || 0.0;
        this.y = y || 0.0;
        this.name = name | "";
        this.counters = [];
    }
}
class Island {
    static create() {
        return new IslandData();
    }
}
class Islands {
    static generateName() {
        let length = Utility.randomInt(1, 3) + Utility.randomInt(1, 3) + Utility.randomInt(1, 3) + 1;
        let isVowel = (Utility.randomInt(0, 1) > 0);
        const vowels = ['a', 'e', 'i', 'o', 'u'];
        const consonants = ['h', 'k', 'l', 'm', 'p'];
        let result = "";
        while (length > 0) {
            if (isVowel) {
                result += vowels[Utility.randomFromArray(vowels)];
            } else {
                result += vowels[Utility.randomFromArray(consonants)];
            }
            isVowel = !isVowel;
            length--;
        }
        return result;
    }
    static create(width, height, minimumDistance, maximumTries) {
        let islands = {}
        let tries = 0;
        while (tries < maximumTries) {
            let name = Islands.generateName();
            let x = Utility.randomRange(0, width);
            let y = Utility.randomRange(0, height);
            let found = false;
            for (let i in islands) {
                let island = islands[i];
                if (Utility.distance(x, y, island.x, island.y) < minimumDistance) {
                    found = true;
                    break;
                }
            }
            if (found) {
                tries++;
            } else {
                islands[name]=new IslandData(name, x, y);
                tries = 0;
            }
        }

        return islands;
    }
}