class MetaData {
    constructor() {
        this.counters = {}
    }
}
class Meta {
    static create() {
        return new MetaData();
    }
    static load() {
        try {
            return JSON.parse(localStorage.getItem(Constants.storage.meta));
        } catch (err) {
            let meta = Meta.create();
            Meta.save(meta);
            return meta;
        }
    }
    static save(meta) {
        localStorage.setItem(Constants.storage.meta, JSON.stringify(meta));
    }
};