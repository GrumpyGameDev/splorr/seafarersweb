class CounterHolder {
    static setCounter(counterName, newValue, holder) {
        holder.counters[counterName] = newValue;
    }
    static getCounter(counterName, holder) {
        return holder.counters[counterName] || 0;
    }
    static changeCounter(counterName, delta, holder) {
        CounterHolder.setCounter(counterName, delta + CounterHolder.getCounter(counterName, holder), holder);
    }
}