class WorldData {
    constructor() {
        this.version = Constants.game.version;
        this.avatar = Avatar.create();
        this.counters = {};
        this.messages = ["The world was formed."];
    }
}
class World {
    static create() {
        return new WorldData();
    }
    static load() {
        let world = Persistence.load(Constants.storage.world, World.create);
        if (world == null || world.version != Constants.game.version) {
            world = World.create();
            Persistence.save(Constants.storage.world, world);
        }
        return world;
    }
    static save(world) {
        Persistence.save(Constants.storage.world, world);
    }
    static reset() {
        Persistence.save(Constants.storage.world, null);
    }
    static clearMessages(world) {
        world.messages = [];
    }
    static addMessage(message, world) {
        world.messages.push(message);
    }
};